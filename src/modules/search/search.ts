export const searchPosition = (word: string, list: Array<string>): number => {
	const position = list.indexOf(word);

	if (position > -1) {
		return position + 1;
	}

	return 0;
};
